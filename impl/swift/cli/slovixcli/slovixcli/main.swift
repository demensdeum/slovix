//
//  main.swift
//  slovix
//
//  Created by Ilia Prokhorov on 13.05.2022.
//

import slovix
import slovixuserdefaultsstorage
import Foundation

let storage = UserDefaultsStorage()

let args = CommandLine.arguments

func show(error: Error) {
    let usage = """
Usage:
slovix get key
slovix set key value
slovix delete key
slovix clear

Example:
slovix set someKey asterix!!
slovix set "ключ длинный на русском" "Вы любите roses?"
slovix get someKey
slovix get "ключ длинный на русском"
"""
    print("❌ Error: \(error.localizedDescription)\n")
    print(usage)
    exit((error as NSError).code.asInt32())
}

do {
    let command = try Command.from(
        command: args.at(1),
        key: args.at(2),
        value: args.at(3)
    )
    do {
        try storage.process(
            command: command,
            output: { output in
                print(output)
            }
        )
    }
    catch {
        show(error: error)
    }
}
catch {
    show(error: error)
}


