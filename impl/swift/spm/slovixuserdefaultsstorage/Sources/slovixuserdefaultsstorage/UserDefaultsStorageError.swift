//
//  File.swift
//  
//
//  Created by Ilia Prokhorov on 16.05.2022.
//

import Foundation

enum UserDefaultsStorageError: Error {
    case noValueFor(key: String)
}

extension UserDefaultsStorageError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noValueFor(let key):
            return "No value for key \(key)"
        }
    }
}

extension UserDefaultsStorageError: CustomNSError {
    public var errorCode: Int {
        switch self {
        case .noValueFor(_):
            return 1
        }
    }
}
