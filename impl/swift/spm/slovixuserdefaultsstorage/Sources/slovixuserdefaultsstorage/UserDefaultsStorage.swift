//
//  UserDefaultsStorage.swift
//  slovix
//
//  Created by Ilia Prokhorov on 13.05.2022.
//

import slovix
import Foundation

public final class UserDefaultsStorage: Storage {
    
    public init() {}
    
    public func process(
        command: Command,
        output: StorageOutput
    ) throws {
        switch command {
        case .get(let key):
            let result = try get(key: key)
            output(result)
        case .set(let key, let value):
            try set(key: key, value: value)
        case .clear:
            try clear()
        case .delete(let key):
            try delete(key: key)
        }
    }
    
    private func get(key: String) throws -> String {
        guard let value = UserDefaults.standard.string(forKey: key) else {
            throw UserDefaultsStorageError.noValueFor(key: key)
        }
        return value
    }
    
    private func set(key: String, value: String) throws {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    private func delete(key: String) throws {
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    private func clear() throws {
        let storage = UserDefaults.standard
        storage.dictionaryRepresentation().forEach{ storage.removeObject(forKey: $0.key) }
    }
}
