//
//  Command.swift
//  slovix
//
//  Created by Ilia Prokhorov on 13.05.2022.
//

import Foundation

public enum Command {
    case `get`(key: String)
    case `set`(key: String, value: String)
    case delete(key: String)
    case clear
    
    static func resolve(key: String?) throws -> String {
        guard let key = key else {
            throw CommandParserError.noKey
        }
        return key
    }
    
    public static func from(
        command: String?,
        key: String?,
        value: String?
    ) throws -> Command {
        
        guard let command = command else {
            throw CommandParserError.nilCommand
        }
        
        switch command {
        case "get":
            return try .get(key: resolve(key: key))
        case "set":
            guard let value = value else {
                throw CommandParserError.noValue
            }
            return try .set(
                key: resolve(
                    key: key
                ),
                value: value
            )
        case "delete":
            return try .delete(key: resolve(key: key))
        case "clear":
            return .clear
        default:
            throw CommandParserError.wrongCommand(command: command)
        }
    }
}
