//
//  File.swift
//  
//
//  Created by Ilia Prokhorov on 16.05.2022.
//

import Foundation

enum CommandParserError: Error {
    case noKey
    case nilCommand
    case noValue
    case wrongCommand(command: String)
}

extension CommandParserError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noKey:
            return "No key provided"
        case .nilCommand:
            return "Nil command"
        case .noValue:
            return "No value provided"
        case .wrongCommand(let command):
            return "Wrong command: \(command)"
        }
    }
}

extension CommandParserError: CustomNSError {
    public var errorCode: Int {
        switch self {
        case .noKey:
            return 1
        case .nilCommand:
            return 2
        case .noValue:
            return 3
        case .wrongCommand(_):
            return 4
        }
    }
}
