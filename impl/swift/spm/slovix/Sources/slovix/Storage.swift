//
//  Storage.swift
//  slovix
//
//  Created by Ilia Prokhorov on 13.05.2022.
//

import Foundation

public protocol Storage {
    func process(
        command: Command,
        output: StorageOutput
    ) throws
}
