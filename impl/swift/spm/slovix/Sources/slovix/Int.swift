//
//  File.swift
//  
//
//  Created by Ilia Prokhorov on 16.05.2022.
//

import Foundation

public extension Int {
    func asInt32() -> Int32 {
        Int32(truncating: self as NSNumber)
    }
}
