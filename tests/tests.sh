validate () {
  if [ $? -eq $testExpectedResult ]
  then
    echo "✅\t$testName test success"
  else
    echo "❌\t$testName test failed"
    exit 1
  fi
}

testExpectedResult=0

testName="Clear"
slovixcli clear
validate

testName="Set ascii"
slovixcli set someKey someValue
validate

testName="Set non ascii"
slovixcli set "какой-то длинный ключ" "Вы любите roses?"
validate

testName="Get ascii"
slovixcli get someKey
validate

testName="Get non ascii"
slovixcli get "какой-то длинный ключ"
validate

testName="Delete ascii"
slovixcli delete someKey
validate

testExpectedResult=1
testName="Validate deleted ascii"
slovixcli get someKey
validate

testExpectedResult=0
testName="Delete non ascii"
slovixcli delete "какой-то длинный ключ"
validate

testExpectedResult=1
testName="Validate deleted non ascii"
slovixcli get someKey
validate

testExpectedResult=4
testName="Wrong command test"
slovixcli wrongCommand 123 999
validate

exit 0
